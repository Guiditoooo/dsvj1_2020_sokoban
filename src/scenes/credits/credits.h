#ifndef CREDITS_H
#define CREDITS_H

#include "extern/extern.h"

using namespace std;

namespace sokoban {

	namespace credits {

		const int baseTextHeight = 30;

			enum class CreditIndex {
				AUDIO,
				ARTIST,
				LIBRARY,
				DEVELOPER
			};

			extern CreditIndex cIndex;

			extern Button button;
			extern Text artist;
			extern Text lib;
			extern Text dev;
			extern Text categ[4];

			static Sound buttonBeep;

			extern bool continueInGame;

			extern void init();
			extern void update();
			extern void draw();
			extern void deinit();

	}

}

#endif