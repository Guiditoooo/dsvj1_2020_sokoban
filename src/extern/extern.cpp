#pragma once
#include "extern.h"

namespace sokoban {

	Resolution winLoseCheck;

	void DrawTX(Texture TX) {
		DrawTexture(TX.TX, TX.pos.x, TX.pos.y, TX.color);
	}

	void DebugLines(Texture TX, Color color) {
		DrawRectangleLinesEx(TX.hitbox, 2, color);
	}

	void LoadMusics() {

		/*shot = LoadSound("res/assets/Sounds/missile.wav");
		BGM = LoadMusicStream("res/assets/Sounds/gameplay.mp3");
		PlayMusicStream(BGM);*/

	}
	void UnloadMusics() {
		//UnloadSound(shot);
	}

	void drawButton(Button btn) {
		DrawRectangleRec(btn.square, btn.color);
		DrawRectangleLinesEx(btn.square, config::squareThickness, btn.txt.color);
		DrawText(&btn.txt.tx[0], btn.txt.pos.x, btn.txt.pos.y, btn.txt.size, btn.txt.color);
	}

	namespace config {

		namespace screen {

			int width = 800;
			int height = 800;
			int fpsRate = 60;

		}

		namespace scenes {

			Scene scene = Scene::NONE;
			Scene next_scene = Scene::MENU;

		}

		Color getRandomColor(int min, int max) {
			
			return { static_cast<unsigned char>(GetRandomValue(min, max)), static_cast<unsigned char>(GetRandomValue(min, max)), static_cast<unsigned char>(GetRandomValue(min,max)), static_cast<unsigned char>(GetRandomValue(200,255)) };

		}

	}

}