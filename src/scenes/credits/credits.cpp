#include "credits.h"

namespace sokoban {

	namespace credits {

		Button button;

		Texture2D logo;

		CreditIndex cIndex;
		Text artist;
		Text lib;
		Text dev;
		Text categ[4];

		Color color;

		void init() {

			buttonBeep = LoadSound("res/Sound/ButtonPup.ogg");

			button.txt.size = config::screen::height * 3 / 80;
			button.square.height = config::screen::height * 3 / 40;
			button.square.width = config::screen::width * 11 / 40;
			button.square.x = config::screen::width * 3 / 4 - MeasureText("MENU", button.txt.size) / 2;
			button.square.y = config::screen::height * 9 / 10 - button.square.height / 2;

			button.txt.pos.x = button.square.x + button.square.width / 2 - (float)MeasureText("MENU", button.txt.size) / 2;
			button.txt.pos.y = button.square.y + button.square.height / 2 - button.txt.size / 2;

			button.color = WHITE;

			color = { static_cast<unsigned char>(GetRandomValue(0,100)),
						static_cast<unsigned char>(GetRandomValue(0,100)),
						static_cast<unsigned char>(GetRandomValue(0,100)),
						200
			};

			categ[static_cast<int>(CreditIndex::ARTIST)].tx = "Artist: Guido Tello";
			categ[static_cast<int>(CreditIndex::ARTIST)].size = config::screen::height / 20;
			categ[static_cast<int>(CreditIndex::ARTIST)].pos.x = config::screen::width * 2 / 20;
			categ[static_cast<int>(CreditIndex::ARTIST)].pos.y = config::screen::height * 9 / 20;

			categ[static_cast<int>(CreditIndex::LIBRARY)].tx = "Library: RayLib";
			categ[static_cast<int>(CreditIndex::LIBRARY)].size = config::screen::height / 20;
			categ[static_cast<int>(CreditIndex::LIBRARY)].pos.x = config::screen::width * 2 / 20;
			categ[static_cast<int>(CreditIndex::LIBRARY)].pos.y = config::screen::height * 11 / 20;

			categ[static_cast<int>(CreditIndex::DEVELOPER)].tx = "Developer: Guido Tello";
			categ[static_cast<int>(CreditIndex::DEVELOPER)].size = config::screen::height / 20;
			categ[static_cast<int>(CreditIndex::DEVELOPER)].pos.x = config::screen::width * 2 / 20;
			categ[static_cast<int>(CreditIndex::DEVELOPER)].pos.y = config::screen::height * 7 / 20;

		}

		void update() {

			Vector2 mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, button.square)) {

				if (!button.selected) {

					PlaySound(buttonBeep);
					button.selected = true;

				}

				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

					config::scenes::next_scene = static_cast<config::scenes::Scene>(config::scenes::Scene::MENU);

				}

			}
			else {

				button.selected = false;

			}

		}

		void draw() {

			BeginDrawing();

			ClearBackground(color);

			DrawText("CREDITS", config::screen::width / 2 - MeasureText("CREDITS", config::screen::height * 3 / 20) / 2, config::screen::height / 20, config::screen::height * 3 / 20, WHITE);

			DrawText(&categ[static_cast<int>(CreditIndex::LIBRARY)].tx[0], categ[static_cast<int>(CreditIndex::LIBRARY)].pos.x, categ[static_cast<int>(CreditIndex::LIBRARY)].pos.y, categ[static_cast<int>(CreditIndex::LIBRARY)].size, categ[static_cast<int>(CreditIndex::LIBRARY)].color);
			DrawText(&categ[static_cast<int>(CreditIndex::ARTIST)].tx[0], categ[static_cast<int>(CreditIndex::ARTIST)].pos.x, categ[static_cast<int>(CreditIndex::ARTIST)].pos.y, categ[static_cast<int>(CreditIndex::ARTIST)].size, categ[static_cast<int>(CreditIndex::ARTIST)].color);
			DrawText(&categ[static_cast<int>(CreditIndex::DEVELOPER)].tx[0], categ[static_cast<int>(CreditIndex::DEVELOPER)].pos.x, categ[static_cast<int>(CreditIndex::DEVELOPER)].pos.y, categ[static_cast<int>(CreditIndex::DEVELOPER)].size, categ[static_cast<int>(CreditIndex::DEVELOPER)].color);

			Button btn = button;
			int heightCorrect = btn.square.height;

			if (btn.selected) {

				btn.txt.color = LIME;
				btn.txt.size *= plusSelected;
				btn.square.height = config::screen::height * 3 / 40;
				btn.square.width = MeasureText(&btn.txt.tx[0],btn.txt.size);
				btn.square.x = config::screen::width * 3 / 4 - MeasureText("MENU", button.txt.size) / 2;
				btn.square.y = config::screen::height * 9 / 10 - button.square.height / 2;

				button.txt.pos.x = button.square.x + button.square.width / 2 - (float)MeasureText("MENU", button.txt.size) / 2;
				button.txt.pos.y = button.square.y + button.square.height / 2 - button.txt.size / 2;

			}
			else {
				button.txt.color = WHITE;
			}
			DrawText("MENU", btn.txt.pos.x, btn.txt.pos.y, btn.txt.size, btn.txt.color);

			EndDrawing();

		}

		void deinit() {

		}

	}

}