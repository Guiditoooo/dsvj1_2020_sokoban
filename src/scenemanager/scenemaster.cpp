#include <iostream>
#include "scenemaster.h"

namespace sokoban {

	namespace config {

		namespace scenes {

			void sceneManager(Scene& scene, Scene nextScene) {

				if (scene != nextScene) {

					switch (scene) //deinit
					{
					case Scene::MENU:
						menu::deinit();
						std::cout << "MENU Deinited";
						break;
					case Scene::GAME:
						game::deinit();
						std::cout << "GAME Deinited";
						break;
					case Scene::CREDITS:
						credits::deinit();
						std::cout << "Credits Deinited";
						break;
					}


					switch (nextScene)//init
					{
					case Scene::MENU:
						menu::init();
						std::cout << "MENU Inited";
						break;
					case Scene::GAME:
						game::init();
						std::cout << "GAME Inited";
						break;
					case Scene::CREDITS:
						credits::init();
						std::cout << "Credits Inited";
						break;
					}

					scene = nextScene;

				}

			}

		}

		void generalInit() {

			InitWindow(screen::width, screen::height, "Random Sokoban");
			SetTargetFPS(screen::fpsRate);
			InitAudioDevice();

		}

		void generalDeinit() {
			CloseAudioDevice();
		}

	}


	void runGame() {

		config::generalInit();

		do {

			using namespace config;
			using namespace scenes;

			scenes::sceneManager(scene, next_scene);

			switch (scene) {

			case Scene::MENU:
				menu::update();
				menu::draw();
				break;
			case Scene::GAME:
				game::update();
				game::draw();
				break;
			case Scene::CREDITS:
				credits::update();
				credits::draw();
				break;
			case Scene::QUIT:
				menu::continueInGame = false;
				break;

			}

		} while (!WindowShouldClose() && menu::continueInGame);

		config::generalDeinit();

	}



}