#include "game.h"
#include <iostream>

namespace sokoban {

	namespace game {

		Rectangle game_area = { config::screen::width / 2 - (config::screen::width * 20 / 22) / 2, config::screen::height / 2 - (config::screen::height * 20 / 22) / 2, config::screen::width * 20 / 22, config::screen::height * 20 / 22 };

		Map lvl[LVL_QTY];

		Tile player;
		Int2 playerPos[LVL_QTY];
		Int2 targetPos[LVL_QTY][MAX_CRATES];

		Texture2D floorTX;
		Texture2D crateTX;
		Texture2D playerTX;
		Texture2D targetTX;

		Sound playerMovS;
		Sound crateMovS;
		Sound buttonS;
		Sound crateInPosS;

		Music gameplayM;

		bool pausing = false;
		bool passingLvl = false;
		bool toMainMenu = false;

		int actualLvl = 0;
		Vector2 tileSize = { (config::screen::width - game_area.x * 2) / 8, (config::screen::height - game_area.y * 2) / 8};
		

		//aux vars
		bool toDoPause = true;
		bool toDoNL = true;
		bool toDoMainMenu = true;
		//

		void init() {

			bool pausing = false;
			bool passingLvl = false;
			bool toMainMenu = false;

			actualLvl = 0;

			//Textures
			floorTX = LoadTexture("res/Floor.png");
			crateTX = LoadTexture("res/Crates.png");
			playerTX = LoadTexture("res/Player.png");
			targetTX = LoadTexture("res/Target.png");

			//Sounds
			playerMovS = LoadSound("res/Sound/PlayerMov.ogg");
			crateMovS = LoadSound("res/Sound/CrateMov.ogg");
			buttonS = LoadSound("res/Sound/ButtonPup.ogg");
			crateInPosS = LoadSound("res/Sound/CrateOnObjetive.ogg");

			//Music
			gameplayM = LoadMusicStream("res/Sound/Gameplay.mp3");
			PlayMusicStream(gameplayM);

			SetSoundVolume(playerMovS, 0.4f);
			SetSoundVolume(crateMovS, 0.4f);
			SetSoundVolume(buttonS, 0.7f);
			SetSoundVolume(crateInPosS, 1.0f);

			player.tileType = MapTiles::PLAYER;

			winLoseCheck = Resolution::PLAYING;

			if (toDoPause) {

				using namespace pause;

				#pragma region PAUSE AREA

				pRec.height = config::screen::height / 2;
				pRec.width = config::screen::width / 2;
				pRec.x = config::screen::width / 2 - pRec.width / 2;
				pRec.y = config::screen::height / 2 - pRec.height / 2;

				pText.tx = "PAUSED";
				pText.size = config::screen::height / 11;
				pText.pos.x = config::screen::width / 2 - MeasureText(&pText.tx[0], pText.size) / 2;
				pText.pos.y = pRec.y + pText.size * 5 / 7;
				pText.color = BLACK;

				#pragma region RESUME

				resume.txt.tx = "RESUME";
				resume.id = menuOptions::RESUME;
				resume.txt.size = config::screen::height * 2 / 39;
				resume.square.width = MeasureText(&resume.txt.tx[0], resume.txt.size) * 3 / 2;
				resume.square.height = resume.txt.size * 2;
				resume.square.x = pRec.x + pRec.width / 2 - resume.square.width / 2;
				resume.square.y = pText.pos.y + pText.size * 3 / 2;
				resume.color = pauseButtonsColor;
				resume.txt.pos.x = resume.square.x + resume.square.width / 2 - MeasureText(&resume.txt.tx[0], resume.txt.size) / 2;
				resume.txt.pos.y = resume.square.y + resume.square.height / 2 - resume.txt.size / 2;
				resume.txt.color = BLACK;

#pragma endregion

				#pragma region MAIN MENU

				mainMenu.txt.tx = "MAIN MENU";
				mainMenu.id = menuOptions::MENU;
				mainMenu.txt.size = config::screen::height * 2 / 39;
				mainMenu.square.width = MeasureText(&mainMenu.txt.tx[0], mainMenu.txt.size) * 3 / 2;
				mainMenu.square.height = mainMenu.txt.size * 2;
				mainMenu.square.x = pRec.x + pRec.width / 2 - mainMenu.square.width / 2;
				mainMenu.square.y = resume.square.y + pText.size * 3 / 2;
				mainMenu.color = pauseButtonsColor;
				mainMenu.txt.pos.x = mainMenu.square.x + mainMenu.square.width / 2 - MeasureText(&mainMenu.txt.tx[0], mainMenu.txt.size) / 2;
				mainMenu.txt.pos.y = mainMenu.square.y + mainMenu.square.height / 2 - mainMenu.txt.size / 2;
				mainMenu.txt.color = BLACK;

				pauseButtons[0] = resume;
				pauseButtons[1] = mainMenu;

#pragma	endregion

				#pragma endregion

			}

			if (toDoNL) {

				using namespace nextLvlMenu;

				#pragma region NEXT LVL AREA

				pRec.height = config::screen::height / 2;
				pRec.width = config::screen::width / 2;
				pRec.x = config::screen::width / 2 - pRec.width / 2;
				pRec.y = config::screen::height / 2 - pRec.height / 2;

				pText.tx = "LVL WON!";
				
				pText.size = config::screen::height / 11;
				pText.pos.x = config::screen::width / 2 - MeasureText(&pText.tx[0], pText.size) / 2;
				pText.pos.y = pRec.y + pText.size * 5 / 7;
				pText.color = BLACK;

#pragma region RESUME

				passLvl.txt.tx = "NEXT LVL";
				passLvl.id = menuOptions::RESUME;
				passLvl.txt.size = config::screen::height * 2 / 39;
				passLvl.square.width = MeasureText(&passLvl.txt.tx[0], passLvl.txt.size) * 3 / 2;
				passLvl.square.height = passLvl.txt.size * 2;
				passLvl.square.x = pRec.x + pRec.width / 2 - passLvl.square.width / 2;
				passLvl.square.y = pText.pos.y + pText.size * 3 / 2;
				passLvl.color = pauseButtonsColor;
				passLvl.txt.pos.x = passLvl.square.x + passLvl.square.width / 2 - MeasureText(&passLvl.txt.tx[0], passLvl.txt.size) / 2;
				passLvl.txt.pos.y = passLvl.square.y + passLvl.square.height / 2 - passLvl.txt.size / 2;
				passLvl.txt.color = BLACK;

#pragma endregion

#pragma region MAIN MENU

				mainMenu.txt.tx = "MAIN MENU";
				mainMenu.id = menuOptions::MENU;
				mainMenu.txt.size = config::screen::height * 2 / 39;
				mainMenu.square.width = MeasureText(&mainMenu.txt.tx[0], mainMenu.txt.size) * 3 / 2;
				mainMenu.square.height = mainMenu.txt.size * 2;
				mainMenu.square.x = pRec.x + pRec.width / 2 - mainMenu.square.width / 2;
				mainMenu.square.y = passLvl.square.y + pText.size * 3 / 2;
				mainMenu.color = pauseButtonsColor;
				mainMenu.txt.pos.x = mainMenu.square.x + mainMenu.square.width / 2 - MeasureText(&mainMenu.txt.tx[0], mainMenu.txt.size) / 2;
				mainMenu.txt.pos.y = mainMenu.square.y + mainMenu.square.height / 2 - mainMenu.txt.size / 2;
				mainMenu.txt.color = BLACK;

				nextLvlButtons[0] = passLvl;
				nextLvlButtons[1] = mainMenu;

#pragma	endregion

#pragma endregion

			}

			if (toDoMainMenu) {

				using namespace backToMenu;

				#pragma region TO MAIN MENU AREA

				pRec.height = config::screen::height / 2;
				pRec.width = config::screen::width / 2;
				pRec.x = config::screen::width / 2 - pRec.width / 2;
				pRec.y = config::screen::height / 2 - pRec.height / 2;

				pText.tx = "YOU WON!";
				pText.size = config::screen::height / 11;
				pText.pos.x = config::screen::width / 2 - MeasureText(&pText.tx[0], pText.size) / 2;
				pText.pos.y = pRec.y + pText.size * 5 / 7;
				pText.color = BLACK;

				#pragma region MAIN MENU

				mainMenu.txt.tx = "MAIN MENU";
				mainMenu.id = menuOptions::MENU;
				mainMenu.txt.size = config::screen::height * 2 / 39;
				mainMenu.square.width = MeasureText(&mainMenu.txt.tx[0], mainMenu.txt.size) * 3 / 2;
				mainMenu.square.height = mainMenu.txt.size * 2;
				mainMenu.square.x = pRec.x + pRec.width / 2 - mainMenu.square.width / 2;
				mainMenu.square.y = pRec.y + pRec.height - mainMenu.txt.size * 9/2;
				mainMenu.color = pauseButtonsColor;
				mainMenu.txt.pos.x = mainMenu.square.x + mainMenu.square.width / 2 - MeasureText(&mainMenu.txt.tx[0], mainMenu.txt.size) / 2;
				mainMenu.txt.pos.y = mainMenu.square.y + mainMenu.square.height / 2 - mainMenu.txt.size / 2;
				mainMenu.txt.color = BLACK;

				backToMenuButton = mainMenu;

#pragma	endregion

#pragma endregion

			}

			for (short i = 0; i < LVL_QTY; i++) {

				int randSprite = GetRandomValue(0, CRATE_SPRITE_QTY);

				for (short j = 0; j < CRATE_QTY_BY_LVL[i]; j++) {

					lvl[i].crates[j].sprite = static_cast<CrateSprite>(randSprite);
				
				}

				for (short x = 0; x < MAX_COLS; x++) {

					for (short y = 0; y < MAX_ROWS; y++) {

						lvl[i].tile[x][y].tileType = MapTiles::FLOOR;

					}

				}

				lvlReset(i);

			}

		}
		
		void update() {

			if (IsKeyPressed(KEY_T)) {

				config::scenes::next_scene = config::scenes::Scene::RESOLUTION;
				winLoseCheck = Resolution::WIN;

			}

			if (IsKeyPressed(KEY_R)) {
				lvlReset(actualLvl);
			}

			if (IsKeyPressed(KEY_P)) {

				pausing = !pausing;
				if (pausing)
					std::cout << "Me pausaste nwn\n";
				else
					std::cout << "Me despausaste uwu\n";


			}
		
			if (!pausing) {

				UpdateMusicStream(gameplayM);

				if (IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_A)){

					if (playerPos[actualLvl].x - 1 >= 0) {

						if (lvl[actualLvl].tile[playerPos[actualLvl].x - 1][playerPos[actualLvl].y].tileType == MapTiles::CRATE ) {

							if (playerPos[actualLvl].x - 2 >= 0) {

								if (lvl[actualLvl].tile[playerPos[actualLvl].x - 2][playerPos[actualLvl].y].tileType == MapTiles::FLOOR || lvl[actualLvl].tile[playerPos[actualLvl].x - 2][playerPos[actualLvl].y].tileType == MapTiles::TARGET) {
									lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y].tileType = MapTiles::FLOOR;
									lvl[actualLvl].tile[playerPos[actualLvl].x - 1][playerPos[actualLvl].y].tileType = MapTiles::PLAYER;
									lvl[actualLvl].tile[playerPos[actualLvl].x - 2][playerPos[actualLvl].y].tileType = MapTiles::CRATE;
									playerPos[actualLvl].x--;
									PlaySound(crateMovS);
								}

							}
						
						}
						else{

							if (lvl[actualLvl].tile[playerPos[actualLvl].x-1][playerPos[actualLvl].y].tileType == MapTiles::FLOOR|| lvl[actualLvl].tile[playerPos[actualLvl].x - 1][playerPos[actualLvl].y].tileType == MapTiles::TARGET) {

								lvl[actualLvl].tile[playerPos[actualLvl].x - 1][playerPos[actualLvl].y].tileType = MapTiles::PLAYER;
								lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y].tileType = MapTiles::FLOOR;
								playerPos[actualLvl].x--;
								PlaySound(playerMovS);

							}

						}
						
					}

				}
				if (IsKeyPressed(KEY_RIGHT) || IsKeyPressed(KEY_D)) {

					if (playerPos[actualLvl].x + 1 < MAX_COLS) {

						if (lvl[actualLvl].tile[playerPos[actualLvl].x + 1][playerPos[actualLvl].y].tileType == MapTiles::CRATE) {

							if (playerPos[actualLvl].x + 2 >= 0) {

								if (lvl[actualLvl].tile[playerPos[actualLvl].x + 2][playerPos[actualLvl].y].tileType == MapTiles::FLOOR || lvl[actualLvl].tile[playerPos[actualLvl].x + 2][playerPos[actualLvl].y].tileType == MapTiles::TARGET) {

									lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y].tileType = MapTiles::FLOOR;
									lvl[actualLvl].tile[playerPos[actualLvl].x + 1][playerPos[actualLvl].y].tileType = MapTiles::PLAYER;
									lvl[actualLvl].tile[playerPos[actualLvl].x + 2][playerPos[actualLvl].y].tileType = MapTiles::CRATE;
									playerPos[actualLvl].x++;
									PlaySound(crateMovS);

								}
							}

						}
						else {

							if (lvl[actualLvl].tile[playerPos[actualLvl].x + 1][playerPos[actualLvl].y].tileType == MapTiles::FLOOR || lvl[actualLvl].tile[playerPos[actualLvl].x + 1][playerPos[actualLvl].y].tileType == MapTiles::TARGET) {

								lvl[actualLvl].tile[playerPos[actualLvl].x + 1][playerPos[actualLvl].y].tileType = MapTiles::PLAYER;
								lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y].tileType = MapTiles::FLOOR;
								playerPos[actualLvl].x++;
								PlaySound(playerMovS);

							}

						}

					}

				}
				if(IsKeyPressed(KEY_UP) || IsKeyPressed(KEY_W)) {

					if (playerPos[actualLvl].y - 1 >= 0) {

						if (lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y-1].tileType == MapTiles::CRATE) {

							if (playerPos[actualLvl].y - 2 >= 0) {

								if (lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y - 2].tileType == MapTiles::FLOOR || lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y - 2].tileType == MapTiles::TARGET) {

									lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y].tileType = MapTiles::FLOOR;
									lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y - 1].tileType = MapTiles::PLAYER;
									lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y - 2].tileType = MapTiles::CRATE;
									playerPos[actualLvl].y--;
									PlaySound(crateMovS);

								}
							}

						}
						else {

							if (lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y-1].tileType == MapTiles::FLOOR || lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y - 1].tileType == MapTiles::TARGET) {

								lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y-1].tileType = MapTiles::PLAYER;
								lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y].tileType = MapTiles::FLOOR;
								playerPos[actualLvl].y--;
								PlaySound(playerMovS);

							}

						}

					}

				}
				if (IsKeyPressed(KEY_DOWN) || IsKeyPressed(KEY_S)) {

					if (playerPos[actualLvl].y + 1 < MAX_ROWS) {

						if (lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y+1].tileType == MapTiles::CRATE) {

							if (playerPos[actualLvl].y + 2 < MAX_ROWS) {

								if (lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y + 2].tileType == MapTiles::FLOOR || lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y + 2].tileType == MapTiles::TARGET) {

									lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y].tileType = MapTiles::FLOOR;
									lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y + 1].tileType = MapTiles::PLAYER;
									lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y + 2].tileType = MapTiles::CRATE;
									playerPos[actualLvl].y++;
									PlaySound(crateMovS);

								}

							}

						}
						else {

							if (lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y+1].tileType == MapTiles::FLOOR || lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y + 1].tileType == MapTiles::TARGET) {

								lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y+1].tileType = MapTiles::PLAYER;
								lvl[actualLvl].tile[playerPos[actualLvl].x][playerPos[actualLvl].y].tileType = MapTiles::FLOOR;
								playerPos[actualLvl].y++;
								PlaySound(playerMovS);

							}

						}

					}

				}

				for (short i = 0; i < CRATE_QTY_BY_LVL[actualLvl]; i++) {
					for (short x = 0; x < MAX_COLS; x++) {
						for (short y = 0; y < MAX_ROWS; y++) {
							if (targetPos[actualLvl][i].x == x && targetPos[actualLvl][i].y == y) {
								if (lvl[actualLvl].tile[x][y].tileType != MapTiles::CRATE) {
									lvl[actualLvl].tile[x][y].tileType = MapTiles::TARGET;
								}
								/*else {
									if (!lvl[actualLvl].crates[i].inPosition) {
										lvl[actualLvl].crates[i].inPosition = true;
										PlaySound(crateInPosS);
									}
								}*/
							}
						}
					}
				}
				int inPosCrates = 0;
				for (short x = 0; x < MAX_COLS; x++) {
					for (short y = 0; y < MAX_ROWS; y++) {
						for (short k = 0; k < CRATE_QTY_BY_LVL[actualLvl]; k++) {
							if (targetPos[actualLvl][k].x == x && targetPos[actualLvl][k].y == y && lvl[actualLvl].tile[x][y].tileType == MapTiles::CRATE) {
								inPosCrates++;
							}
						}
					}
				}

				if (inPosCrates == CRATE_QTY_BY_LVL[actualLvl]) {
					if (actualLvl != LVL_QTY - 1) {
						passingLvl = true;
						NextLvlMenuCheck();
					}
					else
					{
						toMainMenu = true;
						BackToMenuCheck();
					}
				}


			}
			else {

				PauseMenuCheck();

			}

		}
		
		void draw() {

			BeginDrawing();
			ClearBackground(BLACK);

			if (pausing) {

				using namespace pause;

				DrawRectangleRec(pRec, pauseBG);
				DrawRectangleLinesEx(pRec, config::squareThickness, BLACK);
				DrawText(&pText.tx[0], pText.pos.x, pText.pos.y, pText.size, pText.color);

				for (short i = 0; i < howManyPauseButtons; i++) {

					if (pauseButtons[i].selected) {
						pauseButtons[i].color = LIME;
					}
					else {
						pauseButtons[i].color = WHITE;
					}

					drawButton(pauseButtons[i]);

				}

			}
			else if (toMainMenu) {

				using namespace backToMenu;

				DrawRectangleRec(pRec, pauseBG);
				DrawRectangleLinesEx(pRec, config::squareThickness, BLACK);
				DrawText(&pText.tx[0], pText.pos.x, pText.pos.y, pText.size, pText.color);


					if (backToMenuButton.selected) {
						backToMenuButton.color = LIME;
					}
					else {
						backToMenuButton.color = WHITE;
					}

					drawButton(backToMenuButton);

			}
			else if (passingLvl) {

				using namespace nextLvlMenu;

				DrawRectangleRec(pRec, pauseBG);
				DrawRectangleLinesEx(pRec, config::squareThickness, BLACK);
				DrawText(&pText.tx[0], pText.pos.x, pText.pos.y, pText.size, pText.color);

				for (short i = 0; i < howManyNextLvlMenuButtons; i++) {

					if (nextLvlButtons[i].selected) {
						nextLvlButtons[i].color = LIME;
					}
					else {
						nextLvlButtons[i].color = WHITE;
					}

					drawButton(nextLvlButtons[i]);

				}
			}
			else {

				int cratesRemain = 0;

				for (short x = 0; x < MAX_COLS; x++) {

					for (short y = 0; y < MAX_ROWS; y++) {

						switch (lvl[actualLvl].tile[x][y].tileType) {

							case sokoban::game::MapTiles::CRATE:
								/*if (cratesRemain <= CRATE_QTY_BY_LVL[actualLvl] - 1) {
									if (!lvl[actualLvl].crates[cratesRemain].inPosition) {
										DrawTexturePro(crateTX, { 0,static_cast<float>(crateTX.height * static_cast<int>(lvl[actualLvl].crates[cratesRemain].sprite) / 4),100,100 }, { static_cast<float>((x) * (game_area.width / 8) + game_area.x),static_cast<float>((y) * (game_area.height / 8) + game_area.y), tileSize.x + 1 , tileSize.y + 1 }, { 0,0 }, 0, WHITE);
									}
									else {
										DrawTexturePro(crateTX, { 0,static_cast<float>(crateTX.height * static_cast<int>(lvl[actualLvl].crates[cratesRemain].sprite) / 4),100,100 }, { static_cast<float>((x) * (game_area.width / 8) + game_area.x),static_cast<float>((y) * (game_area.height / 8) + game_area.y), tileSize.x + 1 , tileSize.y + 1 }, { 0,0 }, 0, RED);
									}
									cratesRemain++;
								}*/
								DrawTexturePro(crateTX, { 0,static_cast<float>(crateTX.height * static_cast<int>(lvl[actualLvl].crates[cratesRemain].sprite) / 4),100,100 }, { static_cast<float>((x) * (game_area.width / 8) + game_area.x),static_cast<float>((y) * (game_area.height / 8) + game_area.y), tileSize.x + 1 , tileSize.y + 1 }, { 0,0 }, 0, WHITE);
								
								break;
							case sokoban::game::MapTiles::PLAYER:
							case sokoban::game::MapTiles::FLOOR:
								DrawTexturePro(floorTX, { 0,0,100,100 }, { static_cast<float>((x) * (game_area.width / 8) + game_area.x),static_cast<float>((y) * (game_area.height / 8) + game_area.y), tileSize.x + 0.5f , tileSize.y + 0.5f }, { 0,0 }, 0, WHITE);
								break;
							case sokoban::game::MapTiles::TARGET:
								DrawTexturePro(floorTX, { 0,0,100,100 }, { static_cast<float>((x) * (game_area.width / 8) + game_area.x),static_cast<float>((y) * (game_area.height / 8) + game_area.y), tileSize.x + 0.5f , tileSize.y + 0.5f }, { 0,0 }, 0, WHITE);
								DrawTexturePro(targetTX, { 0,0,170,170 }, { static_cast<float>((x) * (game_area.width / 8) + game_area.x),static_cast<float>((y) * (game_area.height / 8) + game_area.y), tileSize.x + 0.5f , tileSize.y + 0.5f }, { 0,0 }, 0, WHITE);
								break;
							case sokoban::game::MapTiles::NONE:
								break;

						}

					}

				}

				

				DrawTexturePro(playerTX, { 0,800,100,100 }, { static_cast<float>((playerPos[actualLvl].x) * (game_area.width / 8) + game_area.x),static_cast<float>((playerPos[actualLvl].y) * (game_area.height / 8) + game_area.y), tileSize.x + 0.5f , tileSize.y + 0.5f }, { 0,0 }, 0, WHITE);

			}

			EndDrawing();

		}
		
		void deinit() {

			UnloadTexture(floorTX);
			UnloadTexture(crateTX);
			UnloadTexture(playerTX);
			UnloadTexture(targetTX);

			UnloadSound(playerMovS);
			UnloadSound(crateMovS);
			UnloadSound(buttonS);

		}

		void PauseMenuCheck() {

			Vector2 mousePoint = GetMousePosition();

			for (int i = 0; i < pause::howManyPauseButtons; i++) {

				if (CheckCollisionPointRec(mousePoint, pause::pauseButtons[i].square)) {

					if (!pause::pauseButtons[i].selected) {
						PlaySound(buttonS);
					}
					pause::pauseButtons[i].selected = true;					

					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

						if (i == 0) {
							config::scenes::next_scene = config::scenes::Scene::GAME;
						}
						else {
							config::scenes::next_scene = config::scenes::Scene::MENU;
						}
						pausing = false;
					}

				}
				else {

					pause::pauseButtons[i].selected = false;

				}

			}
		}

		void NextLvlMenuCheck() {

			using namespace nextLvlMenu;

			Vector2 mousePoint = GetMousePosition();

			for (int i = 0; i < howManyNextLvlMenuButtons; i++) {

				if (CheckCollisionPointRec(mousePoint, nextLvlButtons[i].square)) {

					if (!nextLvlButtons[i].selected) {
						PlaySound(buttonS);
					}
					nextLvlButtons[i].selected = true;

					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

						if (i == 0) {
							actualLvl++;
						}
						else {
							config::scenes::next_scene = config::scenes::Scene::MENU;
							actualLvl = 0;
						}

						passingLvl = false;
						
					}

				}
				else {

					nextLvlButtons[i].selected = false;

				}

			}
		}

		void BackToMenuCheck() {

			using namespace backToMenu;

			Vector2 mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, backToMenuButton.square)) {

				if (!backToMenuButton.selected) {
					PlaySound(buttonS);
				}
				backToMenuButton.selected = true;

				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

					config::scenes::next_scene = config::scenes::Scene::MENU;
					actualLvl = 0;
					passingLvl = false;
					toMainMenu = false;

				}

			}
			else {

				backToMenuButton.selected = false;

			}


		}

		void ResetGrid(int level) {

			for (short x = 0; x < MAX_ROWS; x++) {
				for (short y = 0; y < MAX_COLS; y++) {
					lvl[level].tile[x][y].tileType = MapTiles::NONE;
				}
			}

		}

		void lvlReset(int i) {

			ResetGrid(i);

			switch (i) {
			case 0:
				//FLOOR
				for (short x = 0; x < MAX_ROWS; x++) {
					lvl[i].tile[x][4].tileType = MapTiles::FLOOR;
				}
				//CRATES
				lvl[i].crates[0].pos = { 6,4 };
				//CHARACTER
				playerPos[i] = { 7,4 };
				lvl[i].tile[playerPos[i].x][playerPos[i].y] = player;
				//TARGET
				targetPos[i][0] = { 0,4 };
				break;
			case 2:
				//FLOOR
				lvl[i].tile[2][0].tileType = MapTiles::FLOOR;
				lvl[i].tile[2][1].tileType = MapTiles::FLOOR;
				lvl[i].tile[2][2].tileType = MapTiles::FLOOR;
				lvl[i].tile[2][3].tileType = MapTiles::FLOOR;
				lvl[i].tile[1][3].tileType = MapTiles::FLOOR;
				lvl[i].tile[0][3].tileType = MapTiles::FLOOR;
				lvl[i].tile[2][2].tileType = MapTiles::FLOOR;
				lvl[i].tile[3][2].tileType = MapTiles::FLOOR;
				lvl[i].tile[4][2].tileType = MapTiles::FLOOR;
				lvl[i].tile[5][2].tileType = MapTiles::FLOOR;
				lvl[i].tile[3][3].tileType = MapTiles::FLOOR;
				lvl[i].tile[3][4].tileType = MapTiles::FLOOR;
				lvl[i].tile[3][5].tileType = MapTiles::FLOOR;
				//CRATES
				lvl[i].crates[0].pos = { 2,2 };
				lvl[i].crates[1].pos = { 2,3 };
				lvl[i].crates[2].pos = { 3,4 };
				lvl[i].crates[3].pos = { 4,2 };
				//CHARACTER
				playerPos[i] = { 3,3 };
				lvl[i].tile[playerPos[i].x][playerPos[i].y] = player;
				//TARGET
				targetPos[i][0] = { 2,0 };
				targetPos[i][1] = { 0,3 };
				targetPos[i][2] = { 5,2 };
				targetPos[i][3] = { 3,5 };
				break;
			case 1:
				//FLOOR
				lvl[i].tile[2][2].tileType = MapTiles::FLOOR;
				lvl[i].tile[3][2].tileType = MapTiles::FLOOR;
				lvl[i].tile[4][2].tileType = MapTiles::FLOOR;
				lvl[i].tile[2][3].tileType = MapTiles::FLOOR;
				lvl[i].tile[3][3].tileType = MapTiles::FLOOR;
				lvl[i].tile[4][3].tileType = MapTiles::FLOOR;
				lvl[i].tile[4][4].tileType = MapTiles::FLOOR;
				lvl[i].tile[2][5].tileType = MapTiles::FLOOR;
				lvl[i].tile[3][5].tileType = MapTiles::FLOOR;
				lvl[i].tile[4][5].tileType = MapTiles::FLOOR;
				lvl[i].tile[2][6].tileType = MapTiles::FLOOR;
				lvl[i].tile[3][6].tileType = MapTiles::FLOOR;
				lvl[i].tile[4][6].tileType = MapTiles::FLOOR;
				//CRATES
				lvl[i].crates[0].pos = { 3,3 };
				lvl[i].crates[1].pos = { 3,5 };
				//CHARACTER
				playerPos[i] = { 2,2 };
				lvl[i].tile[playerPos[i].x][playerPos[i].y] = player;
				//TARGET
				targetPos[i][0] = { 2,5 };
				targetPos[i][1] = { 4,6 };
				break;
			case 3:
				//FLOOR
				for (short x = 0; x < 8; x++)
				{
					for (short y = 0; y < 6; y++)
					{

						lvl[i].tile[x][y].tileType = MapTiles::FLOOR;

					}
				}
				lvl[i].tile[3][0].tileType = MapTiles::NONE;
				lvl[i].tile[4][0].tileType = MapTiles::NONE;
				lvl[i].tile[3][4].tileType = MapTiles::NONE;
				lvl[i].tile[4][4].tileType = MapTiles::NONE;
				lvl[i].tile[1][1].tileType = MapTiles::NONE;
				lvl[i].tile[1][3].tileType = MapTiles::NONE;
				lvl[i].tile[1][4].tileType = MapTiles::NONE;
				lvl[i].tile[6][1].tileType = MapTiles::NONE;
				lvl[i].tile[6][2].tileType = MapTiles::NONE;
				lvl[i].tile[6][4].tileType = MapTiles::NONE;
				//CRATES
				lvl[i].crates[0].pos = { 1,2 };
				lvl[i].crates[1].pos = { 6,3 };
				lvl[i].crates[2].pos = { 2,2 };
				lvl[i].crates[3].pos = { 5,2 };
				lvl[i].crates[4].pos = { 2,4 };
				lvl[i].crates[5].pos = { 5,4 };
				//CHARACTER
				playerPos[i] = { 6,0 };
				lvl[i].tile[playerPos[i].x][playerPos[i].y] = player;
				//TARGET
				targetPos[i][0] = { 3,1 };
				targetPos[i][1] = { 3,2 };
				targetPos[i][2] = { 3,3 };
				targetPos[i][3] = { 4,1 };
				targetPos[i][4] = { 4,2 };
				targetPos[i][5] = { 4,3 };
				break;
			}
			for (short f = 0; f < CRATE_QTY_BY_LVL[i]; f++) {
				lvl[i].crates[f].inPosition = false;
				lvl[i].tile[lvl[i].crates[f].pos.x][lvl[i].crates[f].pos.y].tileType = MapTiles::CRATE;
				lvl[i].tile[targetPos[i][f].x][targetPos[i][f].y].tileType = MapTiles::TARGET;
			}
		}

	}

}