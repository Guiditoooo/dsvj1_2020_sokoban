#include "menu.h"
#include <iostream>
#include <string>

namespace sokoban {

	namespace menu {

		bool continueInGame = true;
		bool playedButtonSound = false;
		bool stTimeLoop;

		Button buttons[howManyButtons];
		Vector2 mousePoint = {0,0};

		Music menuM;

		int hoveredOption = 0;
		int hoveredOptionPast = 0;

		Color background;

		void init() {

			//Music
			menuM = LoadMusicStream("res/Sound/Menu.mp3");
			PlayMusicStream(menuM);

			stTimeLoop = true;

			buttonBeep = LoadSound("res/Sound/ButtonPup.ogg");

			title[0].tx = "Random";
			title[1].tx = "Sokoban";

			for (short i = 0; i < 2; i++)
			{
				title[i].size = config::screen::height / 13;
				title[i].pos.x = config::screen::width / 2 - MeasureText(&title[i].tx[0], title->size ) / 2;
				title[i].pos.y = config::screen::height * (2 + i) / 13 + 5 * i;
				title[i].color = config::getRandomColor(200,255);

			}

			background = config::getRandomColor(20,100);

			for (short i = 0; i < howManyButtons; i++) {

				Button btn;
				std::string text;

				switch ((config::scenes::Scene)(i+1)) {

				case config::scenes::Scene::GAME:
					text = "PLAY";
					break;
				case config::scenes::Scene::CREDITS:
					text = "CREDITS";
					break;
				case config::scenes::Scene::QUIT:
					text = "EXIT";
					break;

				}

				btn.txt.tx = text;
				btn.id = (menuOptions)i;

				btn.square.width = (float)MeasureText(&btn.txt.tx[0], baseTextHeight);
				btn.square.height = (float)baseTextHeight;
				btn.square.x = (float)(config::screen::width / 2 - MeasureText(&btn.txt.tx[0], btn.square.height) / 2);
				btn.square.y = config::screen::height * 19 / 40 + config::screen::height * i * 2 / 25;

				btn.color = BLACK;

				buttons[i] = btn;

			}

		}

		void update() {

			if (stTimeLoop) {
				mousePoint = { 0,0 };
				stTimeLoop = false;
			}
			else {
				mousePoint = GetMousePosition();
			}
			
			UpdateMusicStream(menuM);

			for (int i = 0; i < howManyButtons; i++) {

				if (CheckCollisionPointRec(menu::mousePoint, buttons[i].square)) {

					if (!buttons[i].selected) {

						PlaySound(buttonBeep);
						buttons[i].selected = true;

					}

					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

						config::scenes::next_scene = static_cast<config::scenes::Scene>(i + 1);

					}

				}
				else {
					buttons[i].selected = false;
				}

			}

			

		}

		void draw() {

			Color color;

			BeginDrawing();

			ClearBackground(background);

			for (short i = 0; i < titleLenght; i++) {

				DrawText(&title[i].tx[0], title[i].pos.x, title[i].pos.y, title->size, title[i].color);

			}

			for (short i = 0; i < howManyButtons; i++) {

				Button btn = buttons[i];
				int heightCorrect = btn.square.height;

				if (btn.selected) {

					color = LIME;
					btn.square.height *= plusSelected;
					btn.square.width = MeasureText(&btn.txt.tx[0], baseTextHeight);
					btn.square.x = config::screen::width / 2 - MeasureText(&btn.txt.tx[0], btn.square.height) / 2;
					btn.square.y = config::screen::height * 19 / 40 + config::screen::height * i * 2 / 25 - (btn.square.height - heightCorrect) / 2;
					//                      BASE                   +                VARIACION           - DIFERENCIA ENTRE LETRA GRANDE Y CHICA

				}
				else {

					color = btn.color;

				}

				DrawText(&(btn.txt.tx)[0], btn.square.x, btn.square.y, btn.square.height, color);

			}

			EndDrawing();

		}

		void deinit() {

			UnloadSound(buttonBeep);
			UnloadMusicStream(menuM);

		}

	}

}