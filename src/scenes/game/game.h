#ifndef GAME_H
#define GAME_H

#include <string>
#include "raylib.h"
#include "extern/extern.h"

namespace sokoban {

	namespace game {

		extern Rectangle game_area;

		const int MAX_ROWS = 8;
		const int MAX_COLS = 8;

		const int MAX_CRATES = 6;
		const int CRATE_SPRITE_QTY = 4;

		const int LVL_QTY = 4;

		const int CRATE_QTY_BY_LVL[LVL_QTY] = { 1,2,4,6 };

		namespace pause {

			const int howManyPauseButtons = 2;

			static Rectangle pRec;
			static Text pText;

			static Button resume;
			static Button mainMenu;

			static Button pauseButtons[howManyPauseButtons];

		}

		namespace nextLvlMenu {

			const int howManyNextLvlMenuButtons = 2;

			static Rectangle pRec;
			static Text pText;

			static Button passLvl;
			static Button mainMenu;

			static Button nextLvlButtons[howManyNextLvlMenuButtons];

		}

		namespace backToMenu {

			static Rectangle pRec;
			static Text pText;

			static Button mainMenu;

			static Button backToMenuButton;

		}

		enum class CrateSprite{CROSS,LEFT,RIGHT,NORMAL};

		struct Crate {
			CrateSprite sprite = CrateSprite::NORMAL;
			bool inPosition = false;
			Int2 pos;
		};

		enum class MapTiles{CRATE, PLAYER, WALL, FLOOR, TARGET, NONE};

		struct Tile {
			MapTiles tileType;
			Texture2D tx;
		};

		struct Map {
			Crate crates[MAX_CRATES];
			Tile tile[MAX_ROWS][MAX_COLS];
			bool completed = false;
		};

		static const Color pauseBG = { 126, 113, 97, 255 }; //Brown
		static const Color pauseButtonsColor = { 156, 143, 127, 255 }; //LightBrown

		extern bool pausing;
		extern Vector2 tileSize;
		extern int actualLvl;

		void init();
		void update();
		void draw();
		void deinit();
		void PauseMenuCheck();
		void NextLvlMenuCheck();
		void BackToMenuCheck();
		void ResetGrid(int i);
		void lvlReset(int i);

	}

}
#endif