#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H

#include "raylib.h"
#include "extern/extern.h"
#include "scenes/game/game.h"
#include "scenes/menu/menu.h"
#include "scenes/credits/credits.h"

namespace sokoban {

	namespace config {

		namespace scenes {

			void sceneManager(Scene&, Scene);

		}

		void generalInit();
		void generalDeinit();

	}

	void runGame();

}

#endif